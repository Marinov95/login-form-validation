import React from "react";

import classes from "./Navigation.module.css";
import AuthContext from "../../store/auth-context";
import { useContext } from "react";

const Navigation = (props) => {
  const { isLoggedIn, onLogout } = useContext(AuthContext);
  return (
    <nav className={classes.nav}>
      <ul>
        {isLoggedIn && (
          <>
            <li>
              <a href="/">Users</a>
            </li>
            <li>
              <a href="/">Admin</a>
            </li>
            <li>
              <button onClick={onLogout}>Logout</button>
            </li>
          </>
        )}
      </ul>
    </nav>
  );
};

export default Navigation;
