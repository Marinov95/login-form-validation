import React, { useState, useEffect, useReducer, useContext } from "react";

import Card from "../UI/Card/Card";
import classes from "./Login.module.css";
import Button from "../UI/Button/Button";
import AuthContext from "../../store/auth-context";
import { UserActions } from "../../common/Constants";

const passwordReducer = (state, action) => {
  const regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,20}$/;
  switch (action.type) {
    case UserActions.USER_INPUT:
      return { value: action.value, isValid: regex.test(action.value.trim()) };
    case UserActions.INPUT_BLUR: {
      return { value: state.value, isValid: regex.test(state.value) };
    }
    default:
      return { value: "", isValid: false };
  }
};

const emailReducer = (state, action) => {
  const regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  switch (action.type) {
    case UserActions.USER_INPUT: {
      return { value: action.value, isValid: regex.test(action.value) };
    }
    case UserActions.INPUT_BLUR: {
      return { value: state.value, isValid: regex.test(state.value) };
    }
    default:
      return { value: "", isValid: false };
  }
};

const Login = (props) => {
  const { onLogin } = useContext(AuthContext);
  const [formIsValid, setFormIsValid] = useState(false);

  const [emailState, dispatchEmail] = useReducer(emailReducer, {
    value: "",
    isValid: null,
  });

  const [passwordState, dispatchPassword] = useReducer(passwordReducer, {
    value: "",
    isValid: null,
  });

  const { isValid: emailIsValid } = emailState;
  const { isValid: passwordIsValid } = passwordState;

  useEffect(() => {
    const identifier = setTimeout(() => {
      setFormIsValid(emailIsValid && passwordIsValid);
    }, 500);
    return () => {
      clearTimeout(identifier);
    };
  }, [emailIsValid, passwordIsValid]);

  const emailChangeHandler = (event) => {
    dispatchEmail({ type: UserActions.USER_INPUT, value: event.target.value });
  };
  const validateEmailHandler = () => {
    dispatchEmail({ type: UserActions.INPUT_BLUR });
  };

  const passwordChangeHandler = (event) => {
    dispatchPassword({
      type: UserActions.USER_INPUT,
      value: event.target.value,
    });
  };

  const validatePasswordHandler = () => {
    dispatchPassword({ type: UserActions.INPUT_BLUR });
  };

  const submitHandler = (event) => {
    event.preventDefault();
    onLogin(emailState.value, passwordState.value);
  };

  return (
    <Card className={classes.login}>
      <form onSubmit={submitHandler}>
        <div
          className={`${classes.control} ${
            emailState.isValid === false ? classes.invalid : ""
          }`}
        >
          <label htmlFor="email">E-Mail</label>
          <input
            type="email"
            id="email"
            value={emailState.value}
            onChange={emailChangeHandler}
            onBlur={validateEmailHandler}
          />
        </div>
        <div
          className={`${classes.control} ${
            passwordState.isValid === false ? classes.invalid : ""
          }`}
        >
          <label htmlFor="password">Password</label>
          <input
            type="password"
            id="password"
            value={passwordState.value}
            onChange={passwordChangeHandler}
            onBlur={validatePasswordHandler}
          />
        </div>
        <div className={classes.actions}>
          <Button type="submit" className={classes.btn} disabled={!formIsValid}>
            Login
          </Button>
        </div>
      </form>
    </Card>
  );
};

export default Login;
